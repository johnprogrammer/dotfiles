#!/bin/sh

run() {
  if ! pgrep -f "$1" ;
  then
    "$@"&
  fi
}

#Compositor
run picom

#Layouts
run setxkbmap -model pc105 -option "grp:shifts_toggle,compose:sclk" "us,us(intl)"

#Authentication agent
run lxsession

#Redshift + Location
#run /usr/lib/geoclue-2.0/demos/agent
#run redshift-gtk

#Flameshot
run flameshot

#Network manager applet
run nm-applet

#Nvidia Config
#run nvidia-settings --load-config-only

#numlock
run numlockx on

#autolock
run xautolock -locker xsecurelock

#Hide cursor when not in use
run unclutter

# Mouse DPI
run solaar -w hide

#run todoist

run caffeine
