#!/bin/bash

# Install Yay if not already installed
if ! command -v yay &> /dev/null; then
    git clone https://aur.archlinux.org/yay.git
    cd yay
    makepkg -si --noconfirm
    cd ..
    rm -rf yay
fi

# Update Repositories
yay -Syu

# Install packages using Yay from packages.txt
if [ -f packages.txt ]; then
    yay -S --needed --noconfirm $(<packages.txt)
else
    echo "packages.txt not found. No packages to install."
fi

# Create xorg symbolic links
sudo ln -s ~/xorg/* /etc/X11/xorg.conf.d/
